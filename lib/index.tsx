export let allEquals = <A,>(xs: A[]): boolean => {
  if (xs.length < 2) {
    return true;
  }
  return xs.every((x) => x === xs[0]);
};

export let shuffle = <T,>(xs: T[]) => {
  for (let i = xs.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [xs[i], xs[j]] = [xs[j], xs[i]];
  }
  return xs;
};
