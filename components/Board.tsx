import * as R from "rambda";
import { FC } from "react";
import * as L from "../lib";
import * as Card from "./Card";

export type Board = Card.Card[];

export let getStatusAt = (i: number) => (board: Board): Card.Status =>
  R.view(R.lensPath(`${i}.status`), board);

export let setStatusAt = (i: number) => (status: Card.Status) => (
  board: Board
): Board => R.set(R.lensPath(`${i}.status`), status, board);

export let getStatusesBy = (predFn: Card.PredFn) => (
  board: Board
): Card.Status[] =>
  board.flatMap((card: Card.Card) => (predFn(card) ? [card.status] : []));

export let setStatusesBy = (predFn: Card.PredFn) => (status: Card.Status) => (
  board: Board
): Board =>
  board.map((card: Card.Card) => (predFn(card) ? { ...card, status } : card));

export let getSymbolsBy = (predFn: Card.PredFn) => (board: Board): string[] =>
  board.flatMap((card) => (predFn(card) ? [card.symbol] : []));

export let canOpenAt = (i: number) => (board: Board): boolean =>
  i < board.length &&
  Card.isClosed(board[i]) &&
  getStatusesBy(Card.isBlocking)(board).length < 2;

export let areOpensEqual = (board: Board): boolean => {
  let openSymbols = getSymbolsBy(Card.isOpen)(board);
  return openSymbols.length >= 2 && L.allEquals(openSymbols);
};

export let areOpensDifferent = (board: Board): boolean => {
  let openSymbols = getSymbolsBy(Card.isOpen)(board);
  return openSymbols.length >= 2 && !L.allEquals(openSymbols);
};

let charCodeA = "A".charCodeAt(0);

export let makeRandom = (m: number, n: number): Board => {
  if ((m * n) / 2 > 26) throw new Error("too big");
  if ((m * n) % 2) throw new Error("must be even");

  // [0, 1, 2, ...]
  let charCodes = R.range(0, (m * n) / 2);

  // ["A", "B", "C", ...]
  let chars = charCodes.map((i: number) => String.fromCharCode(i + charCodeA));

  // ["A", "A", "B", "B", ...]
  let doubledChars = chars.flatMap((x) => [x, x]);

  // ["A", "C", "B", "D", ...]
  L.shuffle(doubledChars);

  return doubledChars.map((symbol: string) => ({
    symbol,
    status: Card.Status.Closed,
  }));
};

type BoardViewProps = {
  board: Board;
  onClickAt: (i: number) => void;
};

export let BoardView: FC<BoardViewProps> = ({ board, onClickAt }) => {
  return (
    <>
      <div className="board">
        {board.map((card, i) => (
          <Card.CardView key={i} card={card} onClick={(_) => onClickAt(i)} />
        ))}
      </div>
      <style jsx>{`
        .board {
          display: grid;
          grid-template-columns: 1fr 1fr 1fr 1fr;
          grid-template-rows: 1fr 1fr 1fr;
          width: 90vw;
          height: 65vh;
          gap: 2px;
        }
      `}</style>
    </>
  );
};

type ScreenViewProps = {
  background: string;
};

export let ScreenView: FC<ScreenViewProps> = ({ background, children }) => {
  return (
    <>
      <div className="screen">{children}</div>
      <style jsx>{`
        .screen {
          display: flex;
          width: 90vw;
          height: 65vh;
          align-items: center;
          justify-content: center;
          margin: auto;
          cursor: pointer;
          background: ${background};
        }
        :global(.screen h1) {
          font-size: clamp(2rem, 10vw, 5rem);
        }
        :global(.screen p) {
          font-size: clamp(1rem, 3vw, 3rem);
        }
      `}</style>
    </>
  );
};
