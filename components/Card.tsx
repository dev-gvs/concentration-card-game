import * as React from "react";

export enum Status {
  Open,
  Closed,
  Done,
  Failed,
}

export type Card = {
  symbol: string;
  status: Status;
};

export type PredFn = (card: Card) => boolean;

export let isOpen = (card: Card): boolean => card.status == Status.Open;

export let isClosed = (card: Card): boolean => card.status == Status.Closed;

export let isDone = (card: Card): boolean => card.status == Status.Done;

export let isFailed = (card: Card): boolean => card.status == Status.Failed;

export let isBlocking = (card: Card): boolean => isOpen(card) || isFailed(card);

type CardViewProps = {
  card: Card;
  onClick: (event: React.MouseEvent) => void;
};

export let CardView: React.FC<CardViewProps> = ({ card, onClick }) => {
  let { status, symbol } = card;
  return (
    <>
      <div className="card" onClick={onClick}>
        {status == Status.Closed ? "" : symbol}
      </div>
      <style jsx>{`
        .card {
          font-size: clamp(2rem, 10vw, 5rem);
          background: gray;
          display: flex;
          align-items: center;
          justify-content: center;
          min-height: 10vh;
          background: ${statusToBackground(status)};
          cursor: ${status == Status.Closed ? "pointer" : "auto"};
        }
      `}</style>
    </>
  );
};

export let statusToBackground = (status: Status): string => {
  switch (status) {
    case Status.Closed:
      return "darkgray";
    case Status.Open:
      return "#dcdcdc";
    case Status.Done:
      return "#a8db8f";
    case Status.Failed:
      return "#db8f8f";
  }
};
